(deftemplate persona
             (slot nombre (type STRING))
             (slot apellido (type STRING))
             (slot edad (type FLOAT))
             (slot inversioni (type FLOAT))
             (slot patrimonio (type FLOAT))
             (slot objetivo (type STRING))
             (slot ratio (type STRING))
             (slot riesgo (type FLOAT) (default 0.0))
         
)
;(defrule Menu
;
;         =>
;        (printout t "1." crlf)
;        (printout t "2." crlf)
;        (printout t "3.incluir datos" crlf)
;        (bind ?opcion (read))
;        (assert (opcion_menu ?opcion))
;        (if (eq ?opcion 1) then
;                 (assert (recomendar))
;        else (if (eq ?opcion 2) then (assert (menu1)) else (assert (incluird))
;        )
;        )
;
;)

(defrule Incluird
;         (incluird)
         =>
         (printout t "Ponga su nombre: ")
         (bind ?nombrea (read))
         (printout t "ponga su apellido: ")
         (bind ?pape (read))
         (printout t "¿cual es su edad? ")
         (bind ?edad (read))
         (printout t "¿cuanto es su patrimonio? ")
         (bind ?patrimonio (read))
         (printout t "¿cuanto es su inversión inicial? ")
         (bind ?inversioni (read))
         (printout t "¿cual es el objetivo de la inversion? ")
         (bind ?objetivo (read))
         (bind ?ratio (/ ?inversioni ?patrimonio))
         (if (and (> ?edad 18) (< ?edad 30) ) then(bind ?riesgo_e 1.5 ) )
         (if (and (> ?edad 29) (< ?edad 45) ) then(bind ?riesgo_e 1.0 ) )
         (if (and (> ?edad 44) (< ?edad 200) ) then(bind ?riesgo_e 0.5 ) )
         (if (eq ?objetivo vivienda) then(bind ?riesgo_o 0.5 ) )
         (if (eq ?objetivo ahorro) then(bind ?riesgo_o 1 ) )
         (if (eq ?objetivo especulacion) then(bind ?riesgo_o 1.5 ) )
         (bind ?riesgo (+ ?riesgo_e (+ ?riesgo_o (/ 1 ?ratio) ) ) )
(assert (persona (nombre ?nombrea) (apellido ?pape) (edad ?edad) (inversioni ?inversioni) (patrimonio ?patrimonio) (objetivo ?objetivo) (ratio ?ratio) (riesgo ?riesgo) ))

(assert (checki))
)

(defrule chequearInversion
            ?hecho <- (checki)
            (persona (inversioni ?ini) (patrimonio ?pat) (riesgo ?rie))
            =>
            (if (< ?ini (/ ?pat 10)) then (printout t "Cuidado, el riesgo de la inversion es mayor al 20%" crlf) else (printout t "el riesgo de la inversion es: " ?rie crlf ))  
       
(retract ?hecho)
)


;(defrule calculoR
;         ?s <- (persona (edad ?edad) (tipo ?d&~"volatil"&~"estable"))
;            =>
;             (if (and (> ?edad 18) (< ?edad 30)) then (modify ?s (tipo "volatil")) else (modify ?s (tipo "estable"))) 
;
;)  