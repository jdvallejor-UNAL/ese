package ese;

import java.awt.Color; 
import java.awt.BasicStroke; 
import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.*;

import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.data.xy.XYDataset; 
import org.jfree.data.xy.XYSeries; 
import org.jfree.ui.ApplicationFrame; 
import org.jfree.ui.RefineryUtilities; 
import org.jfree.chart.plot.XYPlot; 
import org.jfree.chart.ChartFactory; 
import org.jfree.chart.plot.PlotOrientation; 
import org.jfree.data.xy.XYSeriesCollection; 
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class Grafica extends JPanel {
    
    public ChartPanel chartPanel;
    public TimeSeries serie = new TimeSeries( "Valor Bitcoin USD" );
    
    public Grafica( LinkedList<Double> precio, LinkedList<Second> tiempo ) {
        
        chartPanel = new ChartPanel(createChart(createDataset(precio, tiempo))) {

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(400, 300);
            }
        };
        
        add(chartPanel);
        setVisible(true);
   }
   
    XYDataset createDataset( LinkedList<Double> precio, LinkedList<Second> tiempo ) {
        
        for (int i = 0; i < precio.size(); i++) {
            this.serie.add( tiempo.get(i) , precio.get(i) );
        }          
        final TimeSeriesCollection dataset = new TimeSeriesCollection( );          
        dataset.addSeries( this.serie );          
        return dataset;
    }
   
   JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "Bitcoin", // chart title
            "Time", // x axis label
            "Price USD", // y axis label
            dataset, // data
            false, // include legend
            true, // tooltips
            false // urls
        );
        return chart;
    }
   
}