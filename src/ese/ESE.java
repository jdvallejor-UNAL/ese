package ese;

import java.awt.*;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import jess.ConsolePanel;
import jess.JessException;
import jess.Rete;
import org.jfree.chart.ChartPanel;
import org.jfree.data.time.Second;

import org.apache.commons.math3.distribution.NormalDistribution;

public class ESE extends JFrame {
    
    Container contenedor;
    ConsolePanel jessConsole;
    Grafica graficaBitCoin;
    static LinkedList<Double> valorBitcoin = new LinkedList<Double>();
    static LinkedList<Second> tiempo = new LinkedList<Second>();
    JPanel P1;
    static LinkedList<Double> rendimientos = new LinkedList<Double>();
    static double promedio = 0.0;
    static double standarDev = 0.0;
    static double varianza = 0.0;
    static double deltat = 1.0/252.0;
    static double mediaMuestral = 0.0;
    static double promedioMuestral = 0.0;
    static double varianzaMuestral = 0.0;
    
    public ESE (ConsolePanel cp){
        
        super("ESE - Expert System Exchange");
        
        contenedor = this.getContentPane();
        contenedor.setLayout(new GridLayout(1, 2, 10, 10));
        
        this.jessConsole = cp; 
        contenedor.add(this.jessConsole);
        
        P1 = new JPanel(new BorderLayout());
        this.graficaBitCoin = new Grafica(valorBitcoin, tiempo);
        P1.add(this.graficaBitCoin);
        contenedor.add(this.P1);
        
        this.setSize(1300,700);
	this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    static LinkedList<Double> calcularRendimientos (LinkedList<Double> precios) {
        LinkedList<Double> rendi = new LinkedList<Double>();
        for (int i = 1; i < precios.size(); i++){
            rendi.add(Math.log(precios.get(i)/(precios.get(i-1))));
        }
        return rendi;
    }
    
    static double prom (LinkedList<Double> rendimientos){
        double sum = 0.0;
        for (double rend : rendimientos){
            sum += rend;
        }
        return sum / rendimientos.size();
    }
    
    static double vari(LinkedList<Double> rendimientos){
        double var = 0.0;
        for (double rend : rendimientos){
            var += Math.pow((promedio-rend), 2);
        }
        return var / rendimientos.size();
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        valorBitcoin.add(0.0);
        tiempo.add(new Second());
//        TimeUnit.SECONDS.sleep(1);
//        valorBitcoin.add(431.6);
//        tiempo.add(new Second());
//        TimeUnit.SECONDS.sleep(1);
//        valorBitcoin.add(436.6);
//        tiempo.add(new Second());
//        TimeUnit.SECONDS.sleep(1);
        
        Rete rete = new Rete();
        
        try {
            rete.batch("src/ese/BaseConocimientos.clp");
        } catch (JessException ex) {
            Logger.getLogger(ESE.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ConsolePanel c = new ConsolePanel(rete);
        ESE ese = new ESE(c);
        
        try {
            rete.reset();
            rete.run();
        } catch (JessException ex) {
            Logger.getLogger(ESE.class.getName()).log(Level.SEVERE, null, ex);
        }
        ese.setVisible(true);
        
        Runnable helloRunnable = () -> {
            rendimientos = calcularRendimientos(valorBitcoin);
            promedio = prom(rendimientos);
            varianza = vari(rendimientos);
            standarDev = Math.pow((varianza), 0.5);
            promedioMuestral = deltat * ( promedio - varianza / 2.0 );
            varianzaMuestral = varianza*deltat;
            double valorAleatorio = Math.random()*(1000-200)+200;
            valorBitcoin.add(valorAleatorio);
            Second actual = new Second();
            tiempo.add(actual);
            valorBitcoin.removeFirst();
            tiempo.removeFirst();
            //ese.graficaBitCoin.serie.remove(0);
            ese.graficaBitCoin.serie.add(actual, valorBitcoin.getLast());
        };

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(helloRunnable, 0, 1, TimeUnit.SECONDS);
        
       
        
    }
    
}
